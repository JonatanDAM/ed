package t12p02;

import javax.swing.JOptionPane;
import modelo.ConexionBD;
import t12p02.panelCursoAlta;
import t12p02.panelCursoBaja;
import t12p02.panelCursoListado;
import t12p02.panelAlumnoAlta;
import t12p02.panelAlumnoBaja;

public class T12p01_GUI extends javax.swing.JFrame {

    private panelCursoAlta panelCA;
    private panelCursoBaja panelCB;
    private panelCursoListado panelCL;
    private panelAlumnoAlta panelAA;
    private panelAlumnoBaja panelAB;
    private panelAlumnoListado panelAL;
    private ConexionBD bd;

    public T12p01_GUI() {

        bd = new ConexionBD();

        this.setLocationRelativeTo(null); //Para que aparezca la ventana en el centro.
        panelCA = new panelCursoAlta(bd);
        add(panelCA);
        //pack();

        panelCB = new panelCursoBaja(bd);
        add(panelCB);
        //pack();

        panelCL = new panelCursoListado(bd);
        add(panelCL);
        //pack();
        
        panelAA = new panelAlumnoAlta(bd);
        add(panelAA);
        
        panelAB = new panelAlumnoBaja(bd);
        add(panelAB);

        panelAL = new panelAlumnoListado(bd);
        add(panelAL);
        
        panelCA.setVisible(false);
        panelCB.setVisible(false);
        panelCL.setVisible(false);
        panelAA.setVisible(false);
        panelAB.setVisible(false);
        panelAL.setVisible(false);

        initComponents();

        /* ABRIR CONEXIÓN BD **************************************************/
        try {
            System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            System.out.println("Conexión abierta correctamente.");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error al Abrir la Base de Datos",
                    "ERROR",
                    JOptionPane.ERROR_MESSAGE);
        }
        /**
         * *******************************************************************
         */
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MenuBar = new javax.swing.JMenuBar();
        menuPrincipal = new javax.swing.JMenu();
        menuPrincipal_salir = new javax.swing.JMenuItem();
        menuCursos = new javax.swing.JMenu();
        menuCursos_Alta = new javax.swing.JMenuItem();
        menuCursos_Baja = new javax.swing.JMenuItem();
        menuCursos_Listado = new javax.swing.JMenuItem();
        menuAlumnos = new javax.swing.JMenu();
        menuAlumnos_Alta = new javax.swing.JMenuItem();
        menuAlumnos_Baja = new javax.swing.JMenuItem();
        menuAlumnos_Listado = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 300));
        getContentPane().setLayout(new java.awt.FlowLayout());

        menuPrincipal.setText("Principal");

        menuPrincipal_salir.setText("Salir");
        menuPrincipal_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPrincipal_salirActionPerformed(evt);
            }
        });
        menuPrincipal.add(menuPrincipal_salir);

        MenuBar.add(menuPrincipal);

        menuCursos.setText("Cursos");

        menuCursos_Alta.setText("Alta");
        menuCursos_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Alta);

        menuCursos_Baja.setText("Baja");
        menuCursos_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Baja);

        menuCursos_Listado.setText("Listado");
        menuCursos_Listado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCursos_ActionPerformed(evt);
            }
        });
        menuCursos.add(menuCursos_Listado);

        MenuBar.add(menuCursos);

        menuAlumnos.setText("Alumnos");

        menuAlumnos_Alta.setText("Alta");
        menuAlumnos_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumno_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Alta);

        menuAlumnos_Baja.setText("Baja");
        menuAlumnos_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumno_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Baja);

        menuAlumnos_Listado.setText("Listado");
        menuAlumnos_Listado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlumno_ActionPerformed(evt);
            }
        });
        menuAlumnos.add(menuAlumnos_Listado);

        MenuBar.add(menuAlumnos);

        setJMenuBar(MenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuPrincipal_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPrincipal_salirActionPerformed
        int op = JOptionPane.showConfirmDialog(this,
                "¿Está seguro que desea cerrar la aplicación?",
                this.getTitle(),
                JOptionPane.YES_NO_OPTION);
        switch (op) {
            case JOptionPane.YES_OPTION:
                /* CERRAR CONEXIÓN BD *****************************************/
                try {
                    System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    System.out.println("Conexión cerrada correctamente.");
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "Error al Cerrar la Base de Datos", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                /**
                 * ***********************************************************
                 */
                this.setVisible(false);
                this.dispose();
                break;
        }
    }//GEN-LAST:event_menuPrincipal_salirActionPerformed

    private void menuCursos_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCursos_ActionPerformed
        panelCA.setVisible(false);
        panelCB.setVisible(false);
        panelCL.setVisible(false);
        panelAA.setVisible(false);
        panelAB.setVisible(false);
        panelAL.setVisible(false);
        switch (evt.getActionCommand()) {
            case "Alta":
                panelCA.mostrar();
                break;
            case "Baja":
                panelCB.mostrar();
                break;
            case "Listado":
                panelCL.mostrar();
                break;
        }
    }//GEN-LAST:event_menuCursos_ActionPerformed

    private void menuAlumno_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAlumno_ActionPerformed
        panelCA.setVisible(false);
        panelCB.setVisible(false);
        panelCL.setVisible(false);
        panelAA.setVisible(false);
        panelAB.setVisible(false);
        panelAL.setVisible(false);
        switch (evt.getActionCommand()) {
            case "Alta":
                panelAA.mostrar();
                break;
            case "Baja":
                panelAB.mostrar();
                break;
            case "Listado":
                panelAL.mostrar();
                break;
        }
    }//GEN-LAST:event_menuAlumno_ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(T12p01_GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new T12p01_GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JMenu menuAlumnos;
    private javax.swing.JMenuItem menuAlumnos_Alta;
    private javax.swing.JMenuItem menuAlumnos_Baja;
    private javax.swing.JMenuItem menuAlumnos_Listado;
    private javax.swing.JMenu menuCursos;
    private javax.swing.JMenuItem menuCursos_Alta;
    private javax.swing.JMenuItem menuCursos_Baja;
    private javax.swing.JMenuItem menuCursos_Listado;
    private javax.swing.JMenu menuPrincipal;
    private javax.swing.JMenuItem menuPrincipal_salir;
    // End of variables declaration//GEN-END:variables
}
